import os
import pathlib 
import sys
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from PIL import Image

Path = "F:\\Laptop coding\Alfa TKG\\Alfa AI Team\\Gitlab projects\\2 EDA of folder\\Image data"
path = pathlib.Path(Path)

class extract_data:
    def folders_name(self):
        folders = []
        for files in os.listdir(path):
            if os.path.isfile(files):
                pass
            elif files == "Code":
                pass
            else:
                folders.append(files)
            
        return folders
    
    def files_in_folder(self,folders):
        folders_and_files_loc = dict()
        for folder in folders:
            images = list(path.glob(folder+'/*'))
            folders_and_files_loc[folder] = images

        return folders_and_files_loc

    def len_of_folders(self,folders_and_files_loc):
        len_of_each_folders = dict()
        for details in folders_and_files_loc.keys():
            len_of_each_folders[details] = len(folders_and_files_loc[details])

        return len_of_each_folders
    
    def extract_each_img_data(self,folders_and_files_loc):
        dict_size = dict()
        dict_dim_height = dict()
        dict_dim_width = dict()

        for i in folders_and_files_loc.keys():
            each_size =  []
            height = []
            width = []
            for j in folders_and_files_loc[i]:
                byts = os.path.getsize(j)
                each_size.append(byts / 1024)

                img = Image.open(j)
                height.append(img.height)
                width.append(img.width)

                dict_size[i] = each_size
                dict_dim_height[i] = height
                dict_dim_width[i] = width

        return dict_size, dict_dim_height, dict_dim_width

    def create_data(self,folders,dict_size,dict_dim_height,dict_dim_width):
        for_dataset = []
        for folder in folders:
            data = {"size" : dict_size[folder], "Height":dict_dim_height[folder], "Width":dict_dim_width[folder]}
            for_dataset.append(data)

        return for_dataset

    def create_csv(self,folders,for_dataset):
        for length in range(len(folders)):
            file_name = folders[length]
            d = for_dataset[length]
            with open(f'CSV_data\\{file_name}.csv','w') as dd:
                dd.write("size,height,width\n")
                for iters in range(len(d['size'])):
                    dd.write(f"{d['size'][iters]},{d['Height'][iters]},{d['Width'][iters]}\n")
            dd.close()


    def generate_graph(self): 
        csv = os.listdir('CSV_data')
        for each_csv in csv:
            data = pd.read_csv(f'CSV_data\\{each_csv}')
            sns.displot(x=data['size'], bins=50, color='b', kde=True)
            plt.xlabel("KiloBytes")
            plt.savefig(f'Barchart\\size\\{each_csv.split(".")[0]}.png')

            sns.displot(x=data['height'], bins=50, color='b', kde=True) 
            plt.xlabel("Height dimesnsion")
            plt.savefig(f'Barchart\\height\\{each_csv.split(".")[0]}.png')

            sns.displot(x=data['width'], bins=50, color='b', kde=True)
            plt.xlabel("Width dimesnsion")
            plt.savefig(f'Barchart\\width\\{each_csv.split(".")[0]}.png')




if __name__ == '__main__':
    obj = extract_data()
    folders = obj.folders_name()
    # print(folders)
    folders_and_files_loc = obj.files_in_folder(folders)
    len_of_each_folder = obj.len_of_folders(folders_and_files_loc)
    dict_size, dict_dim_height, dict_dim_width = obj.extract_each_img_data(folders_and_files_loc)
    for_dataset = obj.create_data(folders,dict_size, dict_dim_height, dict_dim_width)
    obj.create_csv(folders,for_dataset)
    obj.generate_graph()